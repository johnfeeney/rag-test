package examples.semantic.app.bak;

import examples.semantic.beans.CustomerBuilder;
import examples.semantic.beans.PremiumCustomer;
import examples.semantic.beans.Subscription;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // customer registration flow

        // build the subscription(s) the customer will be subscribed to
        List<Subscription> subscriptions = new ArrayList<Subscription>();
        Subscription sub1 = new Subscription();
        sub1.setSubscriptionId("1234567");
        sub1.setSubscriptionStatus("active");
        subscriptions.add(sub1);

        // create a new premium customer
        PremiumCustomer customer = new CustomerBuilder(subscriptions)
                .withFirstName("John")
                .withLastName("Doe")
                .age(30)
                .withPhone("123-456-7890")
                .withAddress("123 Main Street")
                .build();

        customer.toString();
    }
}
