package examples.semantic.beans;
public class Customer
{
    public enum CustomerType {FREE, PREMIUM, GOLD};

    protected String firstName;
    protected String lastName;
    protected int age;
    protected String phone;
    protected String address;

    // all getters, and NO setters to provide immutability

    protected String getFirstName() {
        return firstName;
    }

    protected String getLastName() {
        return lastName;
    }

    protected int getAge() {
        return age;
    }

    protected String getPhone() {
        return phone;
    }

    protected String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "User: "
                + this.firstName + ", "
                + this.lastName + ", "
                + this.age + ", "
                + this.phone + ", "
                + this.address;
    }
}