package examples.semantic.beans;

import java.util.List;
import examples.semantic.beans.PremiumCustomer;
import examples.semantic.beans.Subscription;

public class CustomerBuilder
{
    public String firstName;
    public String lastName;
    public int age;
    public String phone;
    public String address;
    public List<Subscription> subscriptions;

    public CustomerBuilder(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public CustomerBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public CustomerBuilder withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public CustomerBuilder age(int age) {
        this.age = age;
        return this;
    }

    public CustomerBuilder withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public CustomerBuilder withAddress(String address) {
        this.address = address;
        return this;
    }

    //Return the finally constructed User object
    public PremiumCustomer build() {
        PremiumCustomer customer= new PremiumCustomer(this);
        validateUserObject(customer);
        return customer;
    }

    private void validateUserObject(PremiumCustomer customer) {
        // do some basic validations to check if user object does not break any assumption of system
        if (this.subscriptions == null || this.subscriptions.isEmpty()) {
            throw new IllegalArgumentException("Subscriptions cannot be empty");
        }
    }
}
