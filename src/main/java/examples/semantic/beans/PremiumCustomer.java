package examples.semantic.beans;

import java.util.List;
import examples.semantic.beans.Customer;
import examples.semantic.beans.Subscription;

public class PremiumCustomer extends Customer
{

    private List<Subscription> subscriptions;

    public PremiumCustomer(CustomerBuilder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.phone = builder.phone;
        this.address = builder.address;
        this.subscriptions = builder.subscriptions;
    }

    // all getters, and NO setters to provide immutability

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Subscription subscription : this.subscriptions) {
            sb.append("Sub ID: ").append(subscription.getSubscriptionId()).append("\n");
            sb.append("Sub status: ").append(subscription.getSubscriptionStatus()).append("\n");
        }
        return super.toString() + sb.toString();
    }
}