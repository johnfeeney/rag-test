package examples.semantic.beans;

public class Subscription {

    private String subscriptionId;
    private String subscriptionName;
    private String subscriptionStatus;

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getAcmeOrgRole() {
        return subscriptionName;
    }

    public void setAcmeOrgRole(String acmeOrgRole) {
        this.subscriptionName = acmeOrgRole;
    }

    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

}
